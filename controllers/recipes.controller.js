const { findById } = require('../models/Recipe.model');
var RecipeService = require('../services/recipe.service');


// Saving the context of this module inside the _the variable
_this = this;

// Async Controller function to get the To do List
exports.getRecipes = async function (req, res, next) {

    // Check the existence of the query parameters, If doesn't exists assign a default value
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10;
    try {
        var Recipes = await RecipeService.getRecipes({}, page, limit)
        // Return the Users list with the appropriate HTTP password Code and Message.
        return res.status(200).json({ status: 200, data: Recipes, message: "Succesfully Recipes Recieved" });
    } catch (e) {
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({ status: 400, message: e.message });
    }
}


exports.getRecipesByUser = async function (req, res, next) {
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10;
    try {
       
        var Recipes = await RecipeService.getRecipes({'recipes.user_id_originante' : req.UserId}, page, limit)
        return res.status(200).json({status: 200, data: Recipes, message: "Succesfully Recipes Recieved"});
  
    } catch (e) {

        return res.status(400).json({status: 400, message: e.message});
    }
}


exports.createRecipe = async function (req, res, next) {
    // Req.Body contains the form submit values.
    console.log("Agregando la receta:", req.body.title)


    var Recipe = {
        title: req.body.title,
        user_id_originante: req.userId, // we save the creater user with the jwt
        difficulty: req.body.difficulty,
        category: req.body.category,
        calories: req.body.calories,
        ingredients: req.body.ingredients,
        procedure: req.body.procedure,
        published: req.body.published,
        carbs: req.body.carbs,
        fat: req.body.fat,
        pictures: req.body.pictures,
        proteins: req.body.proteins,
        update_date: req.body.update_date
    }

    try {
        // Calling the Service function with the new object from the Request Body
        var createdRecipe = await RecipeService.createRecipe(Recipe)
        return res.status(201).json({ createdRecipe, message: "Succesfully Created Recipe" })
    } catch (e) {
        //Return an Error Response Message with Code and the Error Message.
        console.log(e)
        return res.status(400).json({ status: 400, message: "Recipe Creation was Unsuccesfull" })
    }
}

exports.removeRecipe = async function (req, res, next) {
    var id = req.params.id;
    try {
        var deleted = await RecipeService.deleteRecipe(id);
        res.status(200).send("Recipe Succesfully Deleted... ");
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }
}

exports.updateRecipe = async function (req, res, next) {
    // Id is necessary for the update
    if (!req.body._id) {
        return res.status(400).json({ status: 400., message: "_id be present" })
    }

    var Recipe = {
        id: req.body._id ? req.body._id : null,
        title: req.body.title ? req.body.title : null,
        calories: req.body.calories ? req.body.calories : null,
        carbs: req.body.carbs ? req.body.carbs : null,
        fat: req.body.fat ? req.body.fat : null,
        pictures: req.body.pictures ? req.body.pictures : null,
        proteins: req.body.proteins ? req.body.proteins : null,
        difficulty: req.body.difficulty ? req.body.difficulty : null,
        category: req.body.category ? req.body.category : null,
        ingredients: req.body.ingredients ? req.body.ingredients : null,
        procedure: req.body.procedure ? req.body.procedure : null,
        published: req.body.published ? req.body.published : null
    }

    try {
        var updatedRecipe = await RecipeService.updateRecipe(Recipe)

        return res.status(200).json({ status: 200, data: updatedRecipe, message: "Succesfully Updated Recipe" })

    } catch (e) {
        return res.status(400).json({ status: 400., message: e.message })
    }
}

exports.getRecipesById = async function (req, res, next) {
    var id = req.params.id;
    try {
        var Recipes = await RecipeService.getRecipesById(id)
        return res.status(200).json({ status: 200, data: Recipes, message: "Succesfully Recipes returned" });

    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}