// Gettign the Newly created Mongoose Model we just created 
var Recipe = require('../models/Recipe.model');



// Saving the context of this module inside the _the variable
_this = this

// Async function to get the Recipes List
exports.getRecipes = async function (query, page, limit) {
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    // Try Catch the awaited promise to handle the error 
    try {
        console.log("Query", query)
        var Recipes = await Recipe.paginate(query, options)
        // Return the recipes list that was retured by the mongoose promise
        return Recipes;

    } catch (e) {
        // return a Error message describing the reason 
        console.log("error services", e)
        throw Error('Error while Paginating Recipes');
    }
}

exports.createRecipe = async function (recipe) {
    var newRecipe = new Recipe({
        title: recipe.title,
        user_id_originante: recipe.user_id_originante,
        difficulty: recipe.difficulty,
        category: recipe.category,
        calories: recipe.calories,
        ingredients: recipe.ingredients,
        carbs: recipe.carbs,
        fat: recipe.fat,
        procedure: recipe.procedure,
        published: recipe.published,
        pictures: recipe.pictures,
        proteins: recipe.proteins,
        update_date: recipe.update_date,
        created_date: new Date()
    })

    try {
        // Saving the Recipe 
        var savedRecipe = await newRecipe.save();
        return savedRecipe;
    } catch (e) {
        // return a Error message describing the reason 
        console.log(e)
        throw Error("Error while Creating Recipe")
    }
}

exports.deleteRecipe = async function (id) {

    // Delete the Recipe
    try {
        var deleted = await Recipe.deleteOne({
            _id: id
        })
        if (deleted.n === 0 && deleted.ok === 1) {
            throw Error("Recipe Could not be deleted")
        }
        return deleted;
    } catch (e) {
        throw Error("Error Occured while Deleting the Recipe")
    }
}

exports.updateRecipe = async function (recipe) {

    var id = { _id: recipe.id }

    try {
        //Find the old Recipe Object by the Id
        var oldRecipe = await Recipe.findById(id);

    } catch (e) {
        throw Error("the Recipe doenst exist")
    }
    // If no old Recipe Object exists return false
    if (!oldRecipe) {
        return false;
    }

    //Edit the Recipe Object
    oldRecipe.title = recipe.title,
        oldRecipe.calories = recipe.calories,
        oldRecipe.carbs = recipe.carbs,
        oldRecipe.fat = recipe.fat,
        oldRecipe.pictures = recipe.pictures,
        oldRecipe.proteins = recipe.proteins,
        oldRecipe.difficulty = recipe.difficulty,
        oldRecipe.category = recipe.category,
        oldRecipe.ingredients = recipe.ingredients,
        oldRecipe.procedure = recipe.procedure,
        oldRecipe.published = recipe.published,
        oldRecipe.update_date = new Date()

    try {
        var savedRecipe = await oldRecipe.save()
        return savedRecipe;
    } catch (e) {
        throw Error("And Error occured while updating the Recipe");
    }
}

// Async function to get the Recipe by id
exports.getRecipesById = async function (idRecipe) {

    var id = { _id: idRecipe }

    try {
        //Find the old Recipe Object by the Id
        var recipe = await Recipe.findById(id);

        return recipe;
    } catch (e) {
        throw Error("the Recipe doenst exist")
    }
}