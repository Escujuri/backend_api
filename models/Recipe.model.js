var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var RecipeSchema = new mongoose.Schema({
    title: String,
    user_id_originante: String,
    difficulty: String,
    category: String,
    calories: String,
    ingredients: String,
    procedure: String,
    published: Boolean,
    carbs: String,
    fat: String,
    pictures: String,
    proteins: String,
    created_date: Date,
    update_date: Date
})

RecipeSchema.plugin(mongoosePaginate)
const Recipe = mongoose.model('Recipe', RecipeSchema)

module.exports = Recipe;