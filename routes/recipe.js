var express = require('express')
var router = express.Router()
var RecipeController = require('../controllers/recipes.controller');
var Authorization = require('../auth/authorization');


//get all recipes without filter
router.get('/recipes', RecipeController.getRecipes)
router.get('/recipes/id/:id', RecipeController.getRecipesById)


//get all recipes filter and without jwt
router.get('/recipes/:category&:filter', RecipeController.getRecipes)

//with JWT
router.get('/recipes/user', Authorization, RecipeController.getRecipesByUser)
router.post('/recipes/new', Authorization, RecipeController.createRecipe)
router.put('/recipes/update', Authorization, RecipeController.updateRecipe)
router.delete('/recipes/delete/:id', Authorization, RecipeController.removeRecipe)

// Export the Router
module.exports = router;