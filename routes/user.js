var express = require('express')
var router = express.Router()
var UserController = require('../controllers/users.controller');
var Authorization = require('../auth/authorization');

// Authorize each API with middleware and map to the Controller Functions
/* GET users listing. */
router.get('/test', function(req, res, next) {
    res.send('Llegaste a la ruta de  api/user');
  });

router.post('/users/register/', UserController.createUser)
router.post('/users/login/', UserController.loginUser)
router.get('/users' , UserController.getUsers)

//with JWT
router.put('/users/update', Authorization, UserController.updateUser)

// Export the Router
module.exports = router;
